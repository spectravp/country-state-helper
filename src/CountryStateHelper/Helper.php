<?php
/**
 * ZDI Design Group
 * Project country-state-helper
 * Author derekmiranda
 * Date: 8/10/15 11:07 AM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

namespace CountryStateHelper;


class Helper
{
    /**
     * Abbreviation
     */
    const ABBREVIATION = 'ABBREVIATION';

    /**
     * Name
     */
    const NAME  = 'NAME';

    /**
     * Standard numerical array
     */
    const STANDARD_NUMERICAL_ARRAY = 'STANDARD_NUMERICAL_ARRAY';

    /**
     * States
     * @var array
     */
    protected static $states = array(
                    'AL'=>'Alabama',
                    'AK'=>'Alaska',
                    'AZ'=>'Arizona',
                    'AR'=>'Arkansas',
                    'CA'=>'California',
                    'CO'=>'Colorado',
                    'CT'=>'Connecticut',
                    'DE'=>'Delaware',
                    'DC'=>'District of Columbia',
                    'FL'=>'Florida',
                    'GA'=>'Georgia',
                    'HI'=>'Hawaii',
                    'ID'=>'Idaho',
                    'IL'=>'Illinois',
                    'IN'=>'Indiana',
                    'IA'=>'Iowa',
                    'KS'=>'Kansas',
                    'KY'=>'Kentucky',
                    'LA'=>'Louisiana',
                    'ME'=>'Maine',
                    'MD'=>'Maryland',
                    'MA'=>'Massachusetts',
                    'MI'=>'Michigan',
                    'MN'=>'Minnesota',
                    'MS'=>'Mississippi',
                    'MO'=>'Missouri',
                    'MT'=>'Montana',
                    'NE'=>'Nebraska',
                    'NV'=>'Nevada',
                    'NH'=>'New Hampshire',
                    'NJ'=>'New Jersey',
                    'NM'=>'New Mexico',
                    'NY'=>'New York',
                    'NC'=>'North Carolina',
                    'ND'=>'North Dakota',
                    'OH'=>'Ohio',
                    'OK'=>'Oklahoma',
                    'OR'=>'Oregon',
                    'PA'=>'Pennsylvania',
                    'RI'=>'Rhode Island',
                    'SC'=>'South Carolina',
                    'SD'=>'South Dakota',
                    'TN'=>'Tennessee',
                    'TX'=>'Texas',
                    'UT'=>'Utah',
                    'VT'=>'Vermont',
                    'VA'=>'Virginia',
                    'WA'=>'Washington',
                    'WV'=>'West Virginia',
                    'WI'=>'Wisconsin',
                    'WY'=>'Wyoming',
                );

    /**
     * Countries
     * @var array
     */
    protected static $countries = array(
                    'United States',
                    'Afghanistan',
                    'Åland Islands',
                    'Albania',
                    'Algeria',
                    'American Samoa',
                    'Andorra',
                    'Angola',
                    'Anguilla',
                    'Antarctica',
                    'Antigua and Barbuda',
                    'Argentina',
                    'Armenia',
                    'Aruba',
                    'Australia',
                    'Austria',
                    'Azerbaijan',
                    'Bahamas',
                    'Bahrain',
                    'Bangladesh',
                    'Barbados',
                    'Belarus',
                    'Belgium',
                    'Belize',
                    'Benin',
                    'Bermuda',
                    'Bhutan',
                    'Bolivia',
                    'Bosnia and Herzegovina',
                    'Botswana',
                    'Bouvet Island',
                    'Brazil',
                    'British Antarctic Territory',
                    'British Indian Ocean Territory',
                    'British Virgin Islands',
                    'Brunei',
                    'Bulgaria',
                    'Burkina Faso',
                    'Burundi',
                    'Cambodia',
                    'Cameroon',
                    'Canada',
                    'Canton and Enderbury Islands',
                    'Cape Verde',
                    'Cayman Islands',
                    'Central African Republic',
                    'Chad',
                    'Chile',
                    'China',
                    'Christmas Island',
                    'Cocos [Keeling] Islands',
                    'Colombia',
                    'Comoros',
                    'Congo - Brazzaville',
                    'Congo - Kinshasa',
                    'Cook Islands',
                    'Costa Rica',
                    'Croatia',
                    'Cuba',
                    'Cyprus',
                    'Czech Republic',
                    'Côte d’Ivoire',
                    'Denmark',
                    'Djibouti',
                    'Dominica',
                    'Dominican Republic',
                    'Dronning Maud Land',
                    'East Germany',
                    'Ecuador',
                    'Egypt',
                    'El Salvador',
                    'Equatorial Guinea',
                    'Eritrea',
                    'Estonia',
                    'Ethiopia',
                    'Falkland Islands',
                    'Faroe Islands',
                    'Fiji',
                    'Finland',
                    'France',
                    'French Guiana',
                    'French Polynesia',
                    'French Southern Territories',
                    'French Southern and Antarctic Territories',
                    'Gabon',
                    'Gambia',
                    'Georgia',
                    'Germany',
                    'Ghana',
                    'Gibraltar',
                    'Greece',
                    'Greenland',
                    'Grenada',
                    'Guadeloupe',
                    'Guam',
                    'Guatemala',
                    'Guernsey',
                    'Guinea',
                    'Guinea-Bissau',
                    'Guyana',
                    'Haiti',
                    'Heard Island and McDonald Islands',
                    'Honduras',
                    'Hong Kong SAR China',
                    'Hungary',
                    'Iceland',
                    'India',
                    'Indonesia',
                    'Iran',
                    'Iraq',
                    'Ireland',
                    'Isle of Man',
                    'Israel',
                    'Italy',
                    'Jamaica',
                    'Japan',
                    'Jersey',
                    'Johnston Island',
                    'Jordan',
                    'Kazakhstan',
                    'Kenya',
                    'Kiribati',
                    'Kuwait',
                    'Kyrgyzstan',
                    'Laos',
                    'Latvia',
                    'Lebanon',
                    'Lesotho',
                    'Liberia',
                    'Libya',
                    'Liechtenstein',
                    'Lithuania',
                    'Luxembourg',
                    'Macau SAR China',
                    'Macedonia',
                    'Madagascar',
                    'Malawi',
                    'Malaysia',
                    'Maldives',
                    'Mali',
                    'Malta',
                    'Marshall Islands',
                    'Martinique',
                    'Mauritania',
                    'Mauritius',
                    'Mayotte',
                    'Metropolitan France',
                    'Mexico',
                    'Micronesia',
                    'Midway Islands',
                    'Moldova',
                    'Monaco',
                    'Mongolia',
                    'Montenegro',
                    'Montserrat',
                    'Morocco',
                    'Mozambique',
                    'Myanmar [Burma]',
                    'Namibia',
                    'Nauru',
                    'Nepal',
                    'Netherlands',
                    'Netherlands Antilles',
                    'Neutral Zone',
                    'New Caledonia',
                    'New Zealand',
                    'Nicaragua',
                    'Niger',
                    'Nigeria',
                    'Niue',
                    'Norfolk Island',
                    'North Korea',
                    'North Vietnam',
                    'Northern Mariana Islands',
                    'Norway',
                    'Oman',
                    'Pacific Islands Trust Territory',
                    'Pakistan',
                    'Palau',
                    'Palestinian Territories',
                    'Panama',
                    'Panama Canal Zone',
                    'Papua New Guinea',
                    'Paraguay',
                    'Peoples Democratic Republic of Yemen',
                    'Peru',
                    'Philippines',
                    'Pitcairn Islands',
                    'Poland',
                    'Portugal',
                    'Puerto Rico',
                    'Qatar',
                    'Romania',
                    'Russia',
                    'Rwanda',
                    'Réunion',
                    'Saint Barthélemy',
                    'Saint Helena',
                    'Saint Kitts and Nevis',
                    'Saint Lucia',
                    'Saint Martin',
                    'Saint Pierre and Miquelon',
                    'Saint Vincent and the Grenadines',
                    'Samoa',
                    'San Marino',
                    'Saudi Arabia',
                    'Senegal',
                    'Serbia',
                    'Seychelles',
                    'Sierra Leone',
                    'Singapore',
                    'Slovakia',
                    'Slovenia',
                    'Solomon Islands',
                    'Somalia',
                    'South Africa',
                    'South Georgia and the South Sandwich Islands',
                    'South Korea',
                    'Spain',
                    'Sri Lanka',
                    'Sudan',
                    'Suriname',
                    'Svalbard and Jan Mayen',
                    'Swaziland',
                    'Sweden',
                    'Switzerland',
                    'Syria',
                    'São Tomé and Príncipe',
                    'Taiwan',
                    'Tajikistan',
                    'Tanzania',
                    'Thailand',
                    'Timor-Leste',
                    'Togo',
                    'Tokelau',
                    'Tonga',
                    'Trinidad and Tobago',
                    'Tunisia',
                    'Turkey',
                    'Turkmenistan',
                    'Turks and Caicos Islands',
                    'Tuvalu',
                    'U.S. Minor Outlying Islands',
                    'U.S. Miscellaneous Pacific Islands',
                    'U.S. Virgin Islands',
                    'Uganda',
                    'Ukraine',
                    'United Arab Emirates',
                    'United Kingdom',
                    'Uruguay',
                    'Uzbekistan',
                    'Vanuatu',
                    'Vatican City',
                    'Venezuela',
                    'Vietnam',
                    'Wake Island',
                    'Wallis and Futuna',
                    'Western Sahara',
                    'Yemen',
                    'Zambia',
                    'Zimbabwe'
    );

    /**
     * Gets the states in a formatted array
     * @param string $key
     * @param string $value
     * @return array
     * @throws \Exception
     */
    public static function getStates($key = self::ABBREVIATION, $value = self::NAME)
    {
        if( $key == self::ABBREVIATION && $value == self::NAME )
        {
            return self::$states;
        }

        if( $value == self::STANDARD_NUMERICAL_ARRAY )
        {
            throw new \Exception('The value of the array cannot be STANDARD_NUMERICAL_ARRAY. Onlt the $key can.');
        }

        $formattedArray = array();

        foreach(self::$states as $stateAbbreviation => $stateName )
        {
            if( $key == self::NAME && $value == self::NAME )
            {
                $formattedArray[$stateName] = $stateName;
            }
            elseif( $key == self::NAME && $value == self::ABBREVIATION)
            {
                $formattedArray[$stateName] = $stateAbbreviation;
            }
            elseif( $key == self::ABBREVIATION && $value == self::ABBREVIATION)
            {
                $formattedArray[$stateAbbreviation] = $stateAbbreviation;
            }
            elseif( $key == self::STANDARD_NUMERICAL_ARRAY && $value == self::NAME )
            {
                $formattedArray[] = $stateName;
            }
            elseif( $key == self::STANDARD_NUMERICAL_ARRAY && $value == self::ABBREVIATION )
            {
                $formattedArray[] = $stateAbbreviation;
            }
        }

        return $formattedArray;
    }


    /**
     * JSONify the States Array
     * @param $jsonKey
     * @param $jsonValue
     * @param string $stateKey
     * @param string $stateValue
     * @return string
     * @throws \Exception
     */
    public static function getStatesJsonObject($jsonKey, $jsonValue, $stateKey = self::ABBREVIATION, $stateValue = self::NAME)
    {
        $formatted = array();

        foreach(self::getStates($stateKey, $stateValue) as $stateAbbreviation => $stateName )
        {
            $o = new \stdClass();
            $o->$jsonValue = $stateName;
            $o->$jsonKey = $stateAbbreviation;

            $formatted[] = $o;
        }

        return json_encode($formatted);
    }

    /**
     * @param $key
     * @return array
     */
    public static function getCountries($key = self::STANDARD_NUMERICAL_ARRAY)
    {
        if( $key == self::STANDARD_NUMERICAL_ARRAY )
        {
            return self::$countries;
        }

        $formattedArray = [];

        foreach(self::$countries as $country)
        {
            $formattedArray[$country] = $country;
        }

        return $formattedArray;
    }
}