# Country State Helper #

Often times you need a list of US States or Countries. Particularly difficult to maintain is the ```states``` array once you consider the number of variations you might need in an application. This Helper class aims to centralize the data management and make it easy to get formatted arrays of states.

The helper includes two static function:


* ```Helper::getStates()```

* ```Helper::getCountries()```


## Countries ##

The ```Helper::getCountries()``` function accepts one optional parameter describing the format of the array. It defaults to ```STANDARD_NUMERICAL_ARRAY```
 
* ```Helper::getCountries()``` and ```Helper::getCountries(Helper::STANDARD_NUMERICAL_ARRAY)``` returns an array format of: ```0 => 'United States'```

* ```Helper::getCountries(Helper::NAME)``` and ```Helper::getCountries(Helper::ABBREVIATION)``` returns an array format of: ```'United States' => 'United States'```


## States ##

The ```Helper::getStates()``` function accepts two optional parameters describing the format of the array. Parameter one ($key) defaults to ```ABBREVIATION``` and parameter two ($value) defaults to ```NAME```
 
* ```Helper::getStates()``` and ```Helper::getCountries(Helper::ABBREVIATION, Helper::NAME)``` returns an array format of: ```'NY' => 'New York'```

* ```Helper::getStates(Helper::NAME, Helper::NAME)``` returns an array format of: ```'New York' => 'New York'```

* ```Helper::getStates(Helper::NAME, Helper::ABBREVIATION)``` returns an array format of: ```'New York' => 'NY'```

* ```Helper::getStates(Helper::ABBREVIATION, Helper::ABBREVIATION)``` returns an array format of: ```'NY' => 'NY'```

* ```Helper::getStates(Helper::STANDARD_NUMERICAL_ARRAY, Helper::NAME)``` returns an array format of: ```0 => 'New York'```

* ```Helper::getStates(Helper::STANDARD_NUMERICAL_ARRAY, Helper::ABBREVIATION)``` returns an array format of: ```0 => 'NY'```